ruby-batch-loader (2.0.5+dfsg-1) unstable; urgency=medium

  * Team upload

  [ Pirate Praveen ]
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

  [ Cédric Boutillier ]
  * Add patches for ruby3.3 and graphql2 (Closes: #1064754)
  * Bump version dependency on ruby-graphql
  * Remove .rspec_status when cleaning (Closes: #1046483)

 -- Cédric Boutillier <boutil@debian.org>  Sun, 15 Dec 2024 16:51:33 +0100

ruby-batch-loader (2.0.1+dfsg-3) unstable; urgency=medium

  * Team upload.
  * d/control (Build-Depends): Adjust ruby-graphql version (closes: #996159).
    (Depends): Remove ruby-graphql and rely on ${ruby:Depends}.
  * d/copyright (Copyright): Add team.
  * d/rules: Use gem installation layout.
  * d/watch: Update file.
  * d/upstream/metadata: Update some URLs.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 19 Nov 2021 23:38:48 +0100

ruby-batch-loader (2.0.1+dfsg-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Bump debhelper compatibility level to 13

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Aug 2021 02:10:23 +0530

ruby-batch-loader (2.0.1+dfsg-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Cédric Boutillier ]
  * [ci skip] Update team name

  [ Pirate Praveen ]
  * Fix github watch file regexp
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * New upstream version 2.0.1+dfsg
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Apr 2021 00:16:51 +0530

ruby-batch-loader (1.4.1+dfsg.1-3) unstable; urgency=medium

  * Team upload.
  * d/control (Build-Depends): Add ruby-coveralls to fix reprotest.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 28 Feb 2020 10:34:11 +0100

ruby-batch-loader (1.4.1+dfsg.1-2) unstable; urgency=medium

  * Team upload.
  * d/compat: Remove obsolete file.
  * d/control: Add Rules-Requires-Root field.
    (Build-Depends): Use debhelper-compat version 12.
    (Standards-Version): Bump to 4.5.0.
    (Depends): Use ${ruby:Depends}, add graphql version, remove interpreter.
  * d/copyright: Add Upstream-Contact field.
  * d/rules: Add override to install upstream CHANGELOG.md.
  * d/patches/fix-test-for-ruby2.7.patch: Add patch.
    - Fix error expectation to raise FrozenError instead of RuntimeError
      with Ruby 2.7 (closes: #951587).
  * d/patches/series: Add new patch.
  * d/upstream/metadata: Update and complete information.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 28 Feb 2020 10:13:24 +0100

ruby-batch-loader (1.4.1+dfsg.1-1) unstable; urgency=medium

  * Team Upload
  * New upstream
  * Add debian/upstream/metadata
  * Watch file modified redirecting to github
  * Bump Standards-Version to 4.3.0 (no changes needed)

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Thu, 13 Jun 2019 22:26:34 +0530

ruby-batch-loader (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Mon, 17 Dec 2018 17:07:58 +0530

ruby-batch-loader (1.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #893176)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Mar 2018 13:06:55 +0530
